import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_connect/model/country.dart';
import 'package:social_connect/model/messageData.dart';
import 'package:social_connect/model/postData.dart';
import 'package:social_connect/model/userSearchSettings.dart';

//   .snapshots();      // Stream <=
//   .getDocuments();   // Need to do each time <=

class DatabaseMessageService {
  final UserSearchModel user;
  DatabaseMessageService({@required this.user});
  final _messageDatabaseReference = Firestore.instance.collection('messages');

  Future<List<MessageData>> getMessagesAnswerForPost() async {
    var document = await _messageDatabaseReference
        .where('userAnswerUid', isEqualTo: user.uid)
        .getDocuments();
    List<MessageData> messages = [];
    if (document.documents.length > 0) {
      var mapList = document.documents.map((doc) {
        List messagesList = doc['messages'];
        List<ChatData> message = [];
        messagesList.forEach((mes) {
          message.add(ChatData(
            message: mes['message'],
            userEmail: mes['userEmail'],
            date: mes['date'],
            userUid: mes['userUid'],
          ));
        });
        return MessageData(
          postText: doc['postText'],
          userPostUid: doc['userPostUid'],
          userAnswerUid: doc['userAnswerUid'],
          userAnswerViewed: doc['userAnswerViewed'],
          userPostEmail: doc['userPostEmail'],
          userPostViewed: doc['userPostViewed'],
          chatData: message,
        );
      }).toList();
      messages = mapList;
    }
    return messages;
  }

  Future<List<MessageData>> getMessagesForPost() async {
    var document = await _messageDatabaseReference
        .where('userPostUid', isEqualTo: user.uid)
        .getDocuments();

    List<MessageData> messages = [];
    if (document.documents.length > 0) {
      var mapList = document.documents.map((doc) {
        List messagesList = doc['messages'];
        List<ChatData> message = [];
        messagesList.forEach((mes) {
          message.add(ChatData(
            message: mes['message'],
            userEmail: mes['userEmail'],
            date: mes['date'],
            userUid: mes['userUid'],
          ));
        });
        return MessageData(
          postText: doc['postText'],
          userPostUid: doc['userPostUid'],
          userAnswerUid: doc['userAnswerUid'],
          userAnswerViewed: doc['userAnswerViewed'],
          userPostEmail: doc['userPostEmail'],
          userPostViewed: doc['userPostViewed'],
          chatData: message,
        );
      }).toList();
      messages = mapList;
    }
    return messages;
  }

  Future createMessageRecord(String userPostUid, String userPostEmail,
      ChatData chatData, String postText) async {
    return _messageDatabaseReference.document().setData({
      'postText': postText,
      'userPostEmail': userPostEmail,
      'userPostUid': userPostUid,
      'userPostViewed': false,
      'userAnswerUid': user.uid,
      'userAnswerViewed': true,
      'messages': [
        {
          'userUid': user.uid,
          'message': chatData.message,
          'userEmail': user.email,
          'date': DateTime.now(),
        }
      ]
    });
  }

  Future updateViewedStatus(MessageData data) async {
    bool isUserPost = false;
    String anotherUserId = data.userPostUid;
    if (data.userPostUid == user.uid) {
      isUserPost = true;
      anotherUserId = data.userAnswerUid;
    }
    Map<String, dynamic> value = new Map<String, dynamic>();
    if (isUserPost) {
      value['userPostViewed'] = true;
    } else {
      value['userAnswerViewed'] = true;
    }
    return findMessage(isUserPost, anotherUserId).then((docId) {
      if (docId.length > 0) {
        _messageDatabaseReference.document(docId).updateData(value);
      }
    });
  }

  Future updateMessageRecord(MessageData data, ChatData chat, String postText,
      String postEmail) async {
    bool isUserPost = false;
    String anotherUserId = data.userPostUid;
    if (data.userPostUid == user.uid) {
      isUserPost = true;
      anotherUserId = data.userAnswerUid;
    }
    return findMessage(isUserPost, anotherUserId).then((docId) {
      if (docId.length > 0) {
        final updateWithTimestamp = <String, dynamic>{
          'messages': FieldValue.arrayUnion([
            {
              'userUid': user.uid,
              'message': chat.message,
              'userEmail': user.email,
              'date': DateTime.now(),
            }
          ]),
        };
        if (isUserPost) {
          updateWithTimestamp['userAnswerViewed'] = false;
        } else {
          updateWithTimestamp['userPostViewed'] = false;
        }
        _messageDatabaseReference
            .document(docId)
            .updateData(updateWithTimestamp);
      } else {
        _messageDatabaseReference.document().setData({
          'userPostUid': data.userPostUid,
          'userPostEmail': postEmail,
          'userAnswerUid': user.uid,
          'postText': postText,
          'messages': [
            {
              'userUid': user.uid,
              'message': chat.message,
              'userEmail': user.email,
              'date': DateTime.now(),
            }
          ]
        });
      }
    });
  }

  Future<String> findMessage(bool isUserPost, String anotherUserId) async {
    if (isUserPost) {
      var document = await _messageDatabaseReference
          .where('userPostUid', isEqualTo: user.uid)
          .where('userAnswerUid', isEqualTo: anotherUserId)
          .getDocuments();
      var postId = '';
      if (document.documents.length > 0) {
        postId = document.documents.first.documentID;
      }
      return postId;
    } else {
      var document = await _messageDatabaseReference
          .where('userAnswerUid', isEqualTo: user.uid)
          .where('userPostUid', isEqualTo: anotherUserId)
          .getDocuments();
      var postId = '';
      if (document.documents.length > 0) {
        postId = document.documents.first.documentID;
      }
      return postId;
    }
  }
}

class DatabaseService {
  final UserSearchModel user;
  DatabaseService({@required this.user});
  final _countriesDatabaseReference = Firestore.instance.collection('places');
  final _postDatabaseReference = Firestore.instance.collection('posts');

  Future<List<Country>> get countries async {
    var document = await _countriesDatabaseReference.getDocuments();
    var countries = document.documents.map((country) {
      return Country(
          name: country.data['country'], cities: country.data['cities']);
    }).toList();
    return countries;
  }

  Future createRecord(PostData data) async {
    return _postDatabaseReference.document().setData({
      'uid': this.user.uid,
      'date': DateTime.now(),
      'text': data.text,
      'email': data.email,
      'whoareyou': data.whoareyou,
      'whoAreYouLookFor': data.whoAreYouLookFor,
      'goal': data.goal,
      'ageFrom': data.ageFrom,
      'ageTo': data.ageTo,
      'country': data.country,
      'city': data.city,
    });
  }

  Stream<List<PostData>> get posts {
    return _postDatabaseReference
        .orderBy('date', descending: true)
        .snapshots()
        .map(_postListFilterFromSnapshot);
  }

  List<PostData> _postListFilterFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.where((doc) {
      if (user.goal == 0) {
        if (user.city == 'Любой') {
          return doc.data['uid'] != user.uid &&
              doc.data['whoAreYouLookFor'] == user.howAreYou &&
              doc.data['whoareyou'] == user.whoAreYouLooFor &&
              doc.data['country'] == user.country;
        } else {
          if (doc.data['city'] == 'Любой') {
            return doc.data['uid'] != user.uid &&
                doc.data['whoAreYouLookFor'] == user.howAreYou &&
                doc.data['whoareyou'] == user.whoAreYouLooFor &&
                doc.data['country'] == user.country;
          } else {
            return doc.data['uid'] != user.uid &&
                doc.data['whoAreYouLookFor'] == user.howAreYou &&
                doc.data['whoareyou'] == user.whoAreYouLooFor &&
                doc.data['country'] == user.country &&
                doc.data['city'] == user.city;
          }
        }
      } else {
        if (user.city == 'Любой') {
          return doc.data['uid'] != user.uid &&
              doc.data['whoAreYouLookFor'] == user.howAreYou &&
              doc.data['whoareyou'] == user.whoAreYouLooFor &&
              doc.data['country'] == user.country &&
              doc.data['goal'] == user.goal;
        } else {
          if (doc.data['city'] == 'Любой') {
            return doc.data['uid'] != user.uid &&
                doc.data['whoAreYouLookFor'] == user.howAreYou &&
                doc.data['whoareyou'] == user.whoAreYouLooFor &&
                doc.data['country'] == user.country &&
                doc.data['goal'] == user.goal;
          } else {
            return doc.data['uid'] != user.uid &&
                doc.data['whoAreYouLookFor'] == user.howAreYou &&
                doc.data['whoareyou'] == user.whoAreYouLooFor &&
                doc.data['country'] == user.country &&
                doc.data['city'] == user.city &&
                doc.data['goal'] == user.goal;
          }
        }
      }
    }).map((doc) {
      return PostData(
          uid: doc.data['uid'],
          id: doc.documentID,
          date: doc.data['date'] ?? DateTime.now(),
          text: doc.data['text'] ?? '',
          email: doc.data['email'] ?? '',
          whoareyou: doc.data['whoareyou'] ?? 0,
          whoAreYouLookFor: doc.data['whoAreYouLookFor'] ?? 0,
          goal: doc.data['goal'] ?? 0,
          ageFrom: doc.data['ageFrom'] ?? 18,
          ageTo: doc.data['ageTo'] ?? 35,
          country: doc.data['country'] ?? 'Россия',
          city: doc.data['city'] ?? 'Москва');
    }).toList();
  }

  Stream<List<PostData>> get userPosts {
    return _postDatabaseReference
        .orderBy('date', descending: true)
        .snapshots()
        .map(_postListFromSnapshot);
  }

  List<PostData> _postListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents
        .where((doc) => doc.data['uid'] == user.uid)
        .map((doc) {
      return PostData(
          uid: doc.data['uid'],
          id: doc.documentID,
          date: doc.data['date'] ?? DateTime.now(),
          text: doc.data['text'] ?? '',
          email: doc.data['email'] ?? '',
          whoareyou: doc.data['whoareyou'] ?? 0,
          whoAreYouLookFor: doc.data['whoAreYouLookFor'] ?? 0,
          goal: doc.data['goal'] ?? 0,
          ageFrom: doc.data['ageFrom'] ?? 18,
          ageTo: doc.data['ageTo'] ?? 35,
          country: doc.data['country'] ?? 'Россия',
          city: doc.data['city'] ?? 'Москва');
    }).toList();
  }
}
