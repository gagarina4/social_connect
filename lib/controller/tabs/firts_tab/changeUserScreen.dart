import 'package:flutter/material.dart';
import 'package:social_connect/controller/tabs/firts_tab/searchSettingsScreen.dart';
import 'package:social_connect/controller/widgets/addad_cell.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/round_button.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/model/userSeetingEnum.dart';

class ChangeUserScreen extends StatefulWidget {
  final UserSearchModel userSettings;
  ChangeUserScreen({@required this.userSettings});
  @override
  _ChangeUserScreenState createState() => _ChangeUserScreenState();
}

class _ChangeUserScreenState extends State<ChangeUserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Настройки поиска',
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SafeAreaBaseContainer(
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.89,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      AddAdCell(
                        title: 'Кто вы',
                        selectionTitle: widget.userSettings != null
                            ? WhoAreYou
                                .values[widget.userSettings.howAreYou].title
                            : '',
                        index: 0,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Кто вы',
                              index: 0,
                              selectedValue: widget.userSettings.howAreYou,
                              userSettings: widget.userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Кого ищете',
                        selectionTitle: widget.userSettings != null
                            ? WhoAreYouLookFor
                                .values[widget.userSettings.whoAreYouLooFor]
                                .title
                            : '',
                        index: 1,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Кого ищете',
                              index: 1,
                              selectedValue:
                                  widget.userSettings.whoAreYouLooFor,
                              userSettings: widget.userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Цель знакомства',
                        selectionTitle: widget.userSettings != null
                            ? YourGoal.values[widget.userSettings.goal].title
                            : '',
                        index: 2,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Цель знакомства',
                              index: 2,
                              selectedValue: widget.userSettings.goal,
                              userSettings: widget.userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'В возрасте',
                        selectionTitle: widget.userSettings != null
                            ? 'От ${widget.userSettings.ageFrom} до ${widget.userSettings.ageTo} лет'
                            : '',
                        index: 3,
                        onTap: () {
                          routeToSettingsView(
                              title: 'В возрасте',
                              index: 3,
                              selectedValue: 0,
                              userSettings: widget.userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Где',
                        selectionTitle: widget.userSettings != null
                            ? widget.userSettings.country +
                                ', ' +
                                widget.userSettings.city
                            : '',
                        index: 4,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Где',
                              index: 4,
                              selectedValue: 0,
                              userSettings: widget.userSettings);
                        },
                      ),
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 60),
                      child: RoundButton('Найти', () async {
                        Navigator.pop(context);
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void routeToSettingsView(
      {String title,
      int index,
      int selectedValue,
      UserSearchModel userSettings}) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SearchSettingsScreen(
                navTitle: title,
                settingType: SettingType.values[index],
                selectedType: selectedValue,
                userSettings: userSettings,
              ),
          fullscreenDialog: true),
    );
  }
}
