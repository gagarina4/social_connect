import 'package:flutter/material.dart';
import 'package:social_connect/controller/tabs/firts_tab/searchSettingsScreen.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/round_button.dart';

class PremiumScreen extends StatefulWidget {
  @override
  _PremiumScreenState createState() => _PremiumScreenState();
}

class _PremiumScreenState extends State<PremiumScreen> {
  int _purchaseStatus = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Оформить подписку'),
        backgroundColor: Colors.transparent,
      ),
      body: SafeAreaBaseContainer(
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.89,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      SelectionCell(
                        title: '1 неделя за 76₽ в день',
                        isSelected: _purchaseStatus == 0 ? true : false,
                        subtitle: 'Всего за 529₽',
                        index: 0,
                        onTap: (index) {
                          setState(() => _purchaseStatus = 0);
                        },
                      ),
                      SelectionCell(
                        title: '1 месяц за 40₽ в день',
                        isSelected: _purchaseStatus != 0 ? true : false,
                        subtitle: 'Скидка 19%. Всего за 1 190₽',
                        index: 1,
                        onTap: (index) {
                          setState(() => _purchaseStatus = 1);
                        },
                      ),
                    ],
                  ),
                  Center(
                      child: Padding(
                    padding: const EdgeInsets.only(bottom: 60),
                    child: RoundButton('Оплатить', () {}),
                  )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
