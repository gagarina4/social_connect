import 'package:social_connect/Servise/SharedPref.dart';

const userSearchModel_key = 'userSaveString_key';
SharedPref sharedPref = SharedPref();

class UserSearchModel {
  dynamic uid;
  String email = '';
  int howAreYou = 0;
  int whoAreYouLooFor = 1;
  int goal = 2;
  int ageFrom = 18;
  int ageTo = 35;
  String country = 'Россия';
  String city = 'Москва';

  UserSearchModel();

  UserSearchModel.fromJson(Map<String, dynamic> json)
      : uid = json['uid'],
        email = json['email'],
        howAreYou = json['howAreYou'],
        whoAreYouLooFor = json['whoAreYouLooFor'],
        goal = json['goal'],
        ageFrom = json['ageFrom'],
        ageTo = json['ageTo'],
        country = json['country'],
        city = json['city'];

  Map<String, dynamic> toJson() => {
        'uid': uid,
        'email': email,
        'howAreYou': howAreYou,
        'whoAreYouLooFor': whoAreYouLooFor,
        'goal': goal,
        'ageFrom': ageFrom,
        'ageTo': ageTo,
        'country': country,
        'city': city,
      };

  saveUser() {
    sharedPref.save(userSearchModel_key, this);
  }
}
