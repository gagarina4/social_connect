import 'package:firebase_auth/firebase_auth.dart';
import 'package:social_connect/model/user.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Stream<User> get user {
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print('Firebase SignInAnon Fail');
      print(e.toString());
      return null;
    }
  }

  Future signOutAnon() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print('Firebase SignOut Fail');
      print(e.toString());
      return null;
    }
  }

  Future signInWithEmailAndPass(String email, String pass) async {
    try {
      AuthResult result =
          await _auth.signInWithEmailAndPassword(email: email, password: pass);
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print('Firebase SignIn Email and Pass Fail');
      print(e.toString());
      return null;
    }
  }

  Future registerWithEmailAndPass(String email, String pass) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: pass);
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print('Firebase Register Email and Pass Fail');
      print(e.toString());
      return null;
    }
  }

  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }
}
