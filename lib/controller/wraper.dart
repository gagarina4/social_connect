import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/controller/auth/searchScreen.dart';
import 'package:social_connect/controller/tabs/tabBarScreen.dart';
import 'package:social_connect/controller/widgets/loading.dart';
import 'package:social_connect/model/UserSearchSettings.dart';
import 'package:social_connect/model/user.dart';

class Wraper extends StatefulWidget {
  @override
  _WraperState createState() => _WraperState();
}

class _WraperState extends State<Wraper> {
  UserSearchModel userSettings = UserSearchModel();
  var loading = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      loading = true;
    });
    loadSharedPrefs();
  }

  loadSharedPrefs() async {
    try {
      UserSearchModel user =
          UserSearchModel.fromJson(await sharedPref.read(userSearchModel_key));
      setState(() {
        userSettings = user;
        loading = false;
      });
    } catch (Excepetion) {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    if (loading) {
      return Loading();
    } else {
      if (user != null) {
        userSettings.uid = user.uid;
        userSettings.saveUser();
        return Provider<UserSearchModel>.value(
          value: userSettings,
          child: TabBarScreen(),
        );
      } else {
        if (userSettings.uid != null) {
          return Provider<UserSearchModel>.value(
            value: userSettings,
            child: TabBarScreen(),
          );
        } else {
          return Provider<UserSearchModel>.value(
            value: userSettings,
            child: SearchScreen(),
          );
        }
      }
    }
  }
}
