import 'package:cloud_firestore/cloud_firestore.dart';

class PostData {
  String uid;
  String id;
  Timestamp date;
  String text;
  String email;
  int whoareyou;
  int whoAreYouLookFor;
  int goal;
  int ageFrom;
  int ageTo;
  String country;
  String city;

  PostData(
      {this.uid,
      this.id,
      this.date,
      this.text,
      this.email,
      this.whoareyou,
      this.whoAreYouLookFor,
      this.goal,
      this.ageFrom,
      this.ageTo,
      this.country,
      this.city});
}
