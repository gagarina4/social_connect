import 'package:flutter/material.dart';
import 'package:social_connect/controller/tabs/fourth_tab/massega_list.dart';
import 'package:social_connect/model/userSearchSettings.dart';

class MessagesScreen extends StatefulWidget {
  MessagesScreen({@required this.userSettings, @required this.type});
  final UserSearchModel userSettings;
  final int type;
  @override
  _MessagesScreenState createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  @override
  Widget build(BuildContext context) {
    return MessageListView(
      userSettings: widget.userSettings,
      type: widget.type,
    );
  }
}
