import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:social_connect/color_scheme.dart';

class SafeAreaBaseContainer extends StatelessWidget {
  SafeAreaBaseContainer({this.child});
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(33.0),
            topRight: const Radius.circular(33.0),
          ),
          child: Image.asset(
            "images/background_03.png",
            height: double.maxFinite,
            width: double.maxFinite,
            fit: BoxFit.fitHeight,
          ),
        ),
        BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 0.8,
            sigmaY: 0.8,
          ),
          child: Container(
            color: Colors.black.withOpacity(0),
          ),
        ),
        Container(
            decoration: BoxDecoration(
              color: mainViewBackgroundColor,
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(30.0),
                topRight: const Radius.circular(30.0),
              ),
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomCenter,
                stops: [0, 0.5, 0.9],
                colors: [
                  mainViewBackgroundColor,
                  Colors.black.withAlpha(200),
                  mainViewBackgroundColor,
                ],
              ),
            ),
            child: child),
      ],
    );
  }
}
