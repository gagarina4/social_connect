import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/controller/tabs/firts_tab/changeUserScreen.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/post_list.dart';
import 'package:social_connect/model/postData.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/servise/auth.dart';
import 'package:social_connect/servise/database.dart';

class SearchAdsScreen extends StatefulWidget {
  @override
  _SearchAdsScreenState createState() => _SearchAdsScreenState();
}

class _SearchAdsScreenState extends State<SearchAdsScreen> {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    final userSettings = Provider.of<UserSearchModel>(context);

    return StreamProvider<List<PostData>>.value(
      value: DatabaseService(user: userSettings).posts,
      child: Scaffold(
        appBar: buildAppBar(context, userSettings),
        body: SafeAreaBaseContainer(
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: PostList(type: 0),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context, UserSearchModel userSettings) {
    return AppBar(
      title: Text('Объявления'),
      backgroundColor: Colors.transparent,
      actions: <Widget>[
        IconButton(
          onPressed: () async {
            // await _auth.signOutAnon();
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChangeUserScreen(
                        userSettings: userSettings,
                      ),
                  fullscreenDialog: true),
            );
          },
          icon: Icon(Icons.settings),
        )
      ],
    );
  }
}
