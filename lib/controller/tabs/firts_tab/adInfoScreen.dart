import 'package:flutter/material.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_connect/controller/terms/termsScreen.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/loading.dart';
import 'package:social_connect/controller/widgets/round_button.dart';
import 'package:social_connect/controller/widgets/text_style.dart';
import 'package:social_connect/model/messageData.dart';
import 'package:social_connect/model/postData.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/model/userSeetingEnum.dart';
import 'package:social_connect/servise/database.dart';
import 'package:social_connect/servise/mailer.dart';
import 'package:social_connect/servise/time.dart';
import 'dart:io';

class AdInfoScreen extends StatefulWidget {
  AdInfoScreen({@required this.post, @required this.userSettings});
  final PostData post;
  final UserSearchModel userSettings;

  @override
  _AdInfoScreenState createState() => _AdInfoScreenState();
}

class _AdInfoScreenState extends State<AdInfoScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _message = '';
  String _email = '';
  bool _loading = false;
  bool _setEmailFirstTime = true;
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  void _showSnackBar() {
    final snackBar = SnackBar(
      content: Text(
        'Текст сообщения не может быть пустым или содержать одно слово!',
        textAlign: TextAlign.center,
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _showErrorSnackBar() {
    final snackBar = SnackBar(
      content: Text(
        'При отправке сообщения произошла ошибка, проверьте наличие интернета и повторите попытку.',
        textAlign: TextAlign.center,
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      if (widget.userSettings.email.length > 0) {
        if (_setEmailFirstTime) {
          _setEmailFirstTime = false;
          _email = widget.userSettings.email;
        }
      }
    });
    if (_loading) {
      return Loading();
    } else {
      return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Объявление'),
          backgroundColor: Colors.transparent,
        ),
        body: SafeAreaBaseContainer(
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.89,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                        child: TextSubtitle(
                      text: Time.readTimestamp(
                          widget.post.date.millisecondsSinceEpoch),
                      isSubStyle: false,
                    )),
                    SizedBox(
                      height: 12,
                    ),
                    Divider(),
                    SizedBox(
                      height: 6,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text('${widget.post.text}'),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: TextSubtitle(
                        text: WhoAreYou.values[widget.post.whoareyou].title +
                            ', ищет ' +
                            WhoAreYouLookFor
                                .values[widget.post.whoAreYouLookFor].title,
                        isSubStyle: true,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: TextSubtitle(
                        text: YourGoal.values[widget.post.goal].title,
                        isSubStyle: true,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: TextSubtitle(
                        text: widget.post.country + ', ' + widget.post.city,
                        isSubStyle: true,
                      ),
                    ),
                    SizedBox(
                      height: 44,
                    ),
                    Center(
                        child: TextSubtitle(
                      text: 'Напишите ответ',
                      isSubStyle: false,
                    )),
                    Center(
                        child: TextSubtitle(
                      text: 'На данную почту будут приходить ответы',
                      isSubStyle: true,
                    )),
                    SizedBox(
                      height: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Form(
                        // autovalidate: true,
                        key: _formKey,
                        child: TextFormField(
                            validator: validateEmail,
                            // validator: (val) => val.isEmpty ? 'Введите email' : null,
                            onChanged: (val) {
                              setState(() => _email = val);
                            },
                            cursorColor: buttonSelectionColor,
                            initialValue: _email.length > 0 ? _email : null,
                            decoration: InputDecoration(
                              hintText: 'Ваша электронная почта',
                              hintStyle: TextStyle(color: buttonSelectionColor),
                              prefixIcon: Icon(
                                Icons.email,
                                color: Colors.grey,
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 12),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(18)),
                            )),
                      ),
                    ),
                    SizedBox(
                      height: 18,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: TextField(
                          onChanged: (val) {
                            setState(() => _message = val);
                          },
                          maxLength: 200,
                          maxLines: null,
                          cursorColor: buttonSelectionColor,
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.sentences,
                          decoration: InputDecoration(
                            hintText: 'Ваш ответ',
                            hintStyle: TextStyle(color: buttonSelectionColor),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 2.0, horizontal: 12),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(18)),
                          )),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Center(
                      child: CircleAvatar(
                        radius: 64,
                        backgroundImage: _image == null
                            ? NetworkImage('http//')
                            : FileImage(_image),
                        child: Container(
                          width: 128,
                          height: 128,
                          child: FlatButton(
                            shape: CircleBorder(
                              side: BorderSide(
                                width: 1,
                                color: Colors.white54,
                              ),
                            ),
                            onPressed: () async {
                              getImage();
                            },
                            child: _image != null
                                ? Text('Загрузить другое изображение',
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white54),
                                    textAlign: TextAlign.center)
                                : Text('Прикрепить изображение',
                                    style: TextStyle(
                                        fontSize: 13, color: Colors.white54),
                                    textAlign: TextAlign.center),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Center(
                      child: RoundButton(
                        'Отправить',
                        () {
                          if (_message.length < 10) {
                            _showSnackBar();
                          } else {
                            if (_formKey.currentState.validate()) {
                              widget.userSettings.email = _email;
                              widget.userSettings.saveUser();

                              sendMail(_message, (result) async {
                                if (result) {
                                  var chatData = ChatData(
                                    message: _message,
                                    userEmail: widget.userSettings.email,
                                  );
                                  final database = DatabaseMessageService(
                                      user: widget.userSettings);
                                  await database.createMessageRecord(
                                      widget.post.uid,
                                      widget.post.email,
                                      chatData,
                                      widget.post.text);
                                  Navigator.pop(context);
                                } else {
                                  _showErrorSnackBar();
                                }
                              });
                            }
                          }
                        },
                      ),
                    ),
                    SizedBox(height: 12),
                    Center(
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          highlightColor: Colors.transparent,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TermsScreen(),
                                  fullscreenDialog: true),
                            );
                          },
                          child: TextSubtitleAligment(
                            text:
                                'Продолжая вы соглашаетесь\nс нашими правилами',
                            isSubStyle: true,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }
  }

  sendMail(String message, Function(bool) result) {
    setState(() {
      _loading = true;
    });
    Mailer.sendEmail(message, widget.post.email, _image, (res) {
      setState(() {
        _loading = false;
      });
      result(res);
    });
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Введите корректный email';
    else
      return null;
  }
}
