class CellType {
  int index;
  String title;
  CellType(this.index, this.title);
}

enum SettingType { whoareyou, whoareyouloofor, yourGoal, ages, place, city }

extension ExtensionSettingType on SettingType {
  List<CellType> currentCellTypes() {
    switch (this) {
      case SettingType.whoareyou:
        return [
          CellType(WhoAreYou.male.index, WhoAreYou.male.title),
          CellType(WhoAreYou.female.index, WhoAreYou.female.title),
          CellType(WhoAreYou.pilot.index, WhoAreYou.pilot.title),
          CellType(WhoAreYou.doubles.index, WhoAreYou.doubles.title),
          CellType(WhoAreYou.company.index, WhoAreYou.company.title),
        ];
      case SettingType.whoareyouloofor:
        return [
          CellType(WhoAreYouLookFor.male.index, WhoAreYouLookFor.male.title),
          CellType(
              WhoAreYouLookFor.female.index, WhoAreYouLookFor.female.title),
          CellType(WhoAreYouLookFor.pilot.index, WhoAreYouLookFor.pilot.title),
          CellType(
              WhoAreYouLookFor.doubles.index, WhoAreYouLookFor.doubles.title),
          CellType(
              WhoAreYouLookFor.company.index, WhoAreYouLookFor.company.title),
        ];
      case SettingType.yourGoal:
        return [
          CellType(YourGoal.any.index, YourGoal.any.title),
          CellType(YourGoal.relation.index, YourGoal.relation.title),
          CellType(YourGoal.friend.index, YourGoal.friend.title),
          CellType(YourGoal.travel.index, YourGoal.travel.title),
          CellType(YourGoal.virtual.index, YourGoal.virtual.title),
          CellType(YourGoal.sex.index, YourGoal.sex.title),
          CellType(YourGoal.moto.index, YourGoal.moto.title),
          CellType(YourGoal.newyear.index, YourGoal.newyear.title),
        ];
      default:
        return [];
    }
  }
}

enum WhoAreYou { male, female, pilot, doubles, company }

extension ExtensionWhoAreYou on WhoAreYou {
  String get title {
    switch (this) {
      case WhoAreYou.male:
        return 'Парень';
      case WhoAreYou.female:
        return 'Девушка';
      case WhoAreYou.pilot:
        return 'Пилот';
      case WhoAreYou.doubles:
        return 'Двойка';
      case WhoAreYou.company:
        return 'Компания';
      default:
        return '';
    }
  }
}

enum WhoAreYouLookFor { male, female, pilot, doubles, company }

extension ExtensionWhoAreYouLookFor on WhoAreYouLookFor {
  String get title {
    switch (this) {
      case WhoAreYouLookFor.male:
        return 'Парня';
      case WhoAreYouLookFor.female:
        return 'Девушку';
      case WhoAreYouLookFor.pilot:
        return 'Пилота';
      case WhoAreYouLookFor.doubles:
        return 'Двойку';
      case WhoAreYouLookFor.company:
        return 'Компанию';
      default:
        return '';
    }
  }
}

enum YourGoal { any, relation, friend, travel, virtual, sex, moto, newyear }

extension ExtensionYourGoal on YourGoal {
  String get title {
    switch (this) {
      case YourGoal.any:
        return 'Не важно';
      case YourGoal.relation:
        return 'Романтика. Серьезные отношения';
      case YourGoal.friend:
        return 'Дружба и общение';
      case YourGoal.travel:
        return 'Совместные путешествия';
      case YourGoal.virtual:
        return 'Виртуальное общение';
      case YourGoal.sex:
        return 'Секс';
      case YourGoal.moto:
        return 'Покатать на мото';
      case YourGoal.newyear:
        return 'Встретить Новый год';
      default:
        return '';
    }
  }
}
