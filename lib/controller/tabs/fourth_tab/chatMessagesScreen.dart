import 'package:flutter/material.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:image_picker/image_picker.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/loading.dart';
import 'dart:io';

import 'package:social_connect/model/messageData.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/servise/database.dart';
import 'package:social_connect/servise/mailer.dart';

const double heightBotton = 140;

class ChatMessagesScreen extends StatefulWidget {
  final MessageData messageData;
  final UserSearchModel userSettings;
  ChatMessagesScreen({@required this.userSettings, @required this.messageData});
  @override
  _ChatMessagesScreenState createState() => _ChatMessagesScreenState();
}

class _ChatMessagesScreenState extends State<ChatMessagesScreen> {
  File _image;
  bool _loading = false;
  String _message = '';
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future _getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  void _showSnackBar() {
    final snackBar = SnackBar(
      content: Text(
        'Текст сообщения не может быть пустым или содержать одно слово!',
        textAlign: TextAlign.center,
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _showErrorSnackBar() {
    final snackBar = SnackBar(
      content: Text(
        'При отправке сообщения произошла ошибка, проверьте наличие интернета и повторите попытку.',
        textAlign: TextAlign.center,
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _updateViewedStatus() {
    var newMessage = MessageData(
        userPostUid: widget.messageData.userPostUid,
        userAnswerUid: widget.messageData.userAnswerUid);
    final database = DatabaseMessageService(user: widget.userSettings);
    database.updateViewedStatus(newMessage);
  }

  @override
  Widget build(BuildContext context) {
    _updateViewedStatus();
    if (_loading) {
      return Loading();
    } else {
      return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Сообщения'),
          backgroundColor: Colors.transparent,
        ),
        body: SafeAreaBaseContainer(
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: (widget.messageData.chatData.length > 0)
                ? Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: heightBotton),
                        child: ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) =>
                                    const Divider(),
                            itemCount: widget.messageData.chatData.length,
                            itemBuilder: (context, index) {
                              print('user uid');
                              print(widget.userSettings.uid);
                              print(
                                  'messageData uid: ${widget.messageData.chatData[index].userUid}');

                              print(widget.messageData.chatData[index].userUid);
                              return Card(
                                color: Colors.white.withAlpha(20),
                                margin: EdgeInsets.fromLTRB(
                                    (widget.userSettings.uid ==
                                            widget.messageData.chatData[index]
                                                .userUid)
                                        ? 100
                                        : 16,
                                    4,
                                    (widget.userSettings.uid ==
                                            widget.messageData.chatData[index]
                                                .userUid)
                                        ? 16
                                        : 100,
                                    4),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Text(widget
                                      .messageData.chatData[index].message),
                                ),
                              );
                            }),
                      ),
                      Positioned(
                        bottom: 24,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: heightBotton,
                          color: Colors.transparent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              SizedBox(
                                height: 94,
                                child: IconButton(
                                    icon: _image == null
                                        ? Icon(Icons.add_a_photo)
                                        : Icon(Icons.check_circle_outline),
                                    onPressed: () {
                                      _getImage();
                                    }),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.70,
                                child: TextField(
                                  onChanged: (val) {
                                    setState(() => _message = val);
                                  },
                                  maxLength: 200,
                                  maxLines: null,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.text,
                                  textCapitalization:
                                      TextCapitalization.sentences,
                                  cursorColor: buttonSelectionColor,
                                  decoration: InputDecoration(
                                    hintText: 'Ваш ответ',
                                    hintStyle:
                                        TextStyle(color: buttonSelectionColor),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 2.0, horizontal: 12),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(18)),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 94,
                                child: IconButton(
                                  icon: Icon(Icons.send),
                                  onPressed: () async {
                                    if (_message.length < 1) {
                                      _showSnackBar();
                                    } else {
                                      sendMail(
                                        _message,
                                        (result) async {
                                          if (result) {
                                            var newChat = ChatData(
                                              message: _message,
                                              userEmail: _getUserEmail(),
                                            );

                                            var newMessage = MessageData(
                                                userPostUid: widget
                                                    .messageData.userPostUid,
                                                userAnswerUid: widget
                                                    .messageData.userAnswerUid);

                                            final database =
                                                DatabaseMessageService(
                                                    user: widget.userSettings);
                                            await database.updateMessageRecord(
                                                newMessage, newChat, '', '');
                                            Navigator.pop(context, bool);
                                          } else {
                                            _showErrorSnackBar();
                                          }
                                        },
                                      );
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                : Center(
                    child: Text(
                      'Сообщений не найдено',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w700),
                    ),
                  ),
          ),
        ),
      );
    }
  }

  sendMail(String message, Function(bool) result) {
    var sendToEmail = _getUserEmail();
    setState(() {
      _loading = true;
    });
    print('_sendToEmail: $sendToEmail');
    Mailer.sendEmail(message, sendToEmail, _image, (res) {
      setState(() {
        _loading = false;
      });
      result(res);
    });
  }

  String _getUserEmail() {
    var sendToEmail = widget.messageData.userPostEmail;
    for (var message in widget.messageData.chatData.reversed) {
      if (message.userEmail != widget.userSettings.email) {
        sendToEmail = message.userEmail;
        break;
      }
    }
    return sendToEmail;
  }
}
