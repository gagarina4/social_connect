import 'package:cloud_firestore/cloud_firestore.dart';

class MessageData {
  String userPostUid;
  bool userPostViewed;
  String userAnswerUid;
  bool userAnswerViewed;
  String userPostEmail;
  String postText;
  List<ChatData> chatData;

  MessageData(
      {this.userPostUid,
      this.userPostViewed,
      this.userAnswerUid,
      this.userAnswerViewed,
      this.userPostEmail,
      this.postText,
      this.chatData});
}

class ChatData {
  String userUid;
  String message;
  String userEmail;
  Timestamp date;

  ChatData({this.userUid, this.message, this.userEmail, this.date});
}
