import 'package:flutter/material.dart';

const white_01 = Color(0xFFFFFFFF);
const silver_01 = Color(0xFFF6F6F6);

const navbarBackgroundColor = Color(0xFF0E0F0F);
const mainViewBackgroundColor = Color(0xFF090808);
const scaffoldBacgroundColor = silver_01;
const buttonSelectionColor = silver_01;

const unselectedIconTheme = Colors.white24;
const splashColor = Colors.white10;
const splashButtonColor = Colors.black54;
