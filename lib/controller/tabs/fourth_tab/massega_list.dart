import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:social_connect/controller/tabs/fourth_tab/chatMessagesScreen.dart';
import 'package:social_connect/controller/widgets/text_style.dart';
import 'package:social_connect/model/messageData.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/servise/database.dart';

class MessageListView extends StatefulWidget {
  MessageListView({@required this.userSettings, @required this.type});
  final UserSearchModel userSettings;
  final int type;
  @override
  _MessageListViewState createState() => _MessageListViewState();
}

class _MessageListViewState extends State<MessageListView> {
  List<MessageData> messages = [];
  double _opacity = 0;

  RefreshController _refreshController =
      RefreshController(initialRefresh: true);

  void _onRefresh() async {
    await _getMessages();
    setState(() {
      _opacity = 1;
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await _getMessages();
    setState(() {
      _opacity = 1;
    });
    _refreshController.loadComplete();
  }

  Future _getMessages() async {
    final database = DatabaseMessageService(user: widget.userSettings);
    if (widget.type == 0) {
      database.getMessagesForPost().then((value) {
        setState(() {
          messages = value;
        });
      });
    } else {
      database.getMessagesAnswerForPost().then((value) {
        setState(() {
          messages = value;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      footer: CustomFooter(
        builder: (BuildContext context, LoadStatus mode) {
          Widget body;
          if (mode == LoadStatus.idle) {
            body = Text("Обновить данные");
          } else if (mode == LoadStatus.loading) {
            body = CupertinoActivityIndicator();
          } else if (mode == LoadStatus.failed) {
            body = Text('Ошибка загрузки данных, повторите попытку');
          } else if (mode == LoadStatus.canLoading) {
            body = Text("Обновить");
          } else {
            body = Text("Нет данных");
          }
          return Container(
            height: 55.0,
            child: Center(child: body),
          );
        },
      ),
      controller: _refreshController,
      onRefresh: _onRefresh,
      onLoading: _onLoading,
      child: ListView.separated(
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: messages.length > 0 ? messages.length : 1,
        itemBuilder: (context, index) {
          return Container(
            child: Material(
              color: Colors.transparent,
              child: messages.length > 0
                  ? InkWell(
                      highlightColor: Colors.transparent,
                      splashColor: splashColor,
                      child: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: cellFor(messages[index]),
                      ),
                      onTap: () async {
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ChatMessagesScreen(
                              userSettings: widget.userSettings,
                              messageData: messages[index],
                            ),
                          ),
                        ).then((val) {
                          _refreshController.requestRefresh();
                        });
                      },
                    )
                  : Opacity(
                      opacity: _opacity,
                      child: Container(
                          margin: EdgeInsets.fromLTRB(12, 44, 12, 0),
                          child: Center(child: Text('У вас нет сообщений.'))),
                    ),
            ),
          );
        },
      ),
    );
  }

  Widget cellFor(MessageData data) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 4),
          TextSubtitle(
            text: 'Текст объявления:',
            isSubStyle: true,
          ),
          SizedBox(height: 2),
          Text(data.postText),
          SizedBox(height: 2),
          NewMessages(data, data.userPostUid == widget.userSettings.uid),
          SizedBox(height: 4),
        ],
      ),
    );
  }

  Widget NewMessages(MessageData data, bool isPostUser) {
    if (isPostUser) {
      return TextSubtitle(
        text: data.userPostViewed
            ? 'Нет новых сообщений'
            : 'Есть новые сообщения',
        isSubStyle: data.userPostViewed ? true : false,
      );
    } else {
      return TextSubtitle(
        text: data.userAnswerViewed
            ? 'Нет новых сообщений'
            : 'Есть новые сообщения',
        isSubStyle: data.userAnswerViewed ? true : false,
      );
    }
  }
}
