import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'color_scheme.dart';
import 'package:social_connect/controller/wraper.dart';
import 'package:social_connect/servise/auth.dart';
import 'package:social_connect/model/user.dart';

void main() => runApp(
      Application(),
    );

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
          title: 'social_connect',
          debugShowCheckedModeBanner: false,
          home: Wraper(),
          theme: ThemeData.dark().copyWith(
            dividerColor: Colors.white10,
            appBarTheme: _appBarTheme(),
            scaffoldBackgroundColor: scaffoldBacgroundColor,
          )),
    );
  }
}

AppBarTheme _appBarTheme() {
  return AppBarTheme(
    brightness: Brightness.light,
    elevation: 0.0,
    color: mainViewBackgroundColor,
    iconTheme: IconThemeData(
      color: mainViewBackgroundColor,
    ),
    textTheme: TextTheme(
      title: TextStyle(
          color: mainViewBackgroundColor,
          fontFamily: 'OpenSans',
          fontSize: 19,
          fontWeight: FontWeight.w600),
    ),
  );
}
