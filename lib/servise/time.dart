import 'package:intl/intl.dart';

class Time {
  static String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
    var diff = now.difference(date);
    var time = '';
    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date).toString() + ', Cегодня';
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = 'Вчера';
      } else {
        time = 'На этой неделе';
      }
    } else {
      if (diff.inDays == 7) {
        time = 'Неделю назад';
      } else {
        time = 'Более недели назад';
      }
    }
    return time;
  }
}
