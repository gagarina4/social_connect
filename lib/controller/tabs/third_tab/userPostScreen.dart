import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/controller/tabs/third_tab/premiumScreen.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/post_list.dart';
import 'package:social_connect/model/postData.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/servise/database.dart';

class UserPostScreen extends StatefulWidget {
  @override
  _UserPostScreenState createState() => _UserPostScreenState();
}

class _UserPostScreenState extends State<UserPostScreen> {
  @override
  Widget build(BuildContext context) {
    final userSettings = Provider.of<UserSearchModel>(context);
    return StreamProvider<List<PostData>>.value(
      value: DatabaseService(user: userSettings).userPosts,
      child: Scaffold(
        appBar: buildAppBar(context),
        body: SafeAreaBaseContainer(
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: PostList(type: 10),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Мои объявления'),
      backgroundColor: Colors.transparent,
      actions: <Widget>[
        IconButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PremiumScreen(),
                  fullscreenDialog: true),
            );
          },
          icon: Icon(Icons.help),
        )
      ],
    );
  }
}
