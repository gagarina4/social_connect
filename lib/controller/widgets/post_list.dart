import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:social_connect/controller/widgets/loading.dart';
import 'package:social_connect/controller/widgets/post_tile.dart';
import 'package:social_connect/model/postData.dart';

class PostList extends StatefulWidget {
  final int type;
  PostList({@required this.type});
  @override
  _PostListState createState() => _PostListState();
}

class _PostListState extends State<PostList> {
  @override
  Widget build(BuildContext context) {
    final posts = Provider.of<List<PostData>>(context);
    if (posts == null) {
      return Loading();
    }
    if (posts.length > 0) {
      return ListView.separated(
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: posts.length,
        itemBuilder: (context, index) {
          return PostTile(post: posts[index], type: widget.type);
        },
      );
    } else {
      return Stack(
        children: <Widget>[
          Positioned(
            bottom: 20,
            left: 0,
            right: 0,
            child: widget.type == 0
                ? Text('')
                : Icon(
                    Icons.arrow_downward,
                    color: buttonSelectionColor,
                  ),
          ),
          Container(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  height: 40,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        widget.type == 0
                            ? 'Объявлений не найдено'
                            : 'У вас нет объявлений.',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w700),
                      ),
                      Text(
                        widget.type == 0
                            ? 'Измените настройки поиска'
                            : 'Добавьте новое.',
                        style: TextStyle(color: Colors.white60),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    }
  }
}
