import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/controller/tabs/firts_tab/searchSettingsScreen.dart';
import 'package:social_connect/controller/widgets/addad_cell.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/round_button.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/model/userSeetingEnum.dart';
import 'package:social_connect/servise/auth.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    final userSettings = Provider.of<UserSearchModel>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Настройки поиска',
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SafeAreaBaseContainer(
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.89,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      AddAdCell(
                        title: 'Кто вы',
                        selectionTitle: userSettings != null
                            ? WhoAreYou.values[userSettings.howAreYou].title
                            : '',
                        index: 0,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Кто вы',
                              index: 0,
                              selectedValue: userSettings.howAreYou,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Кого ищете',
                        selectionTitle: userSettings != null
                            ? WhoAreYouLookFor
                                .values[userSettings.whoAreYouLooFor].title
                            : '',
                        index: 1,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Кого ищете',
                              index: 1,
                              selectedValue: userSettings.whoAreYouLooFor,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Цель знакомства',
                        selectionTitle: userSettings != null
                            ? YourGoal.values[userSettings.goal].title
                            : '',
                        index: 2,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Цель знакомства',
                              index: 2,
                              selectedValue: userSettings.goal,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'В возрасте',
                        selectionTitle: userSettings != null
                            ? 'От ${userSettings.ageFrom} до ${userSettings.ageTo} лет'
                            : '',
                        index: 3,
                        onTap: () {
                          routeToSettingsView(
                              title: 'В возрасте',
                              index: 3,
                              selectedValue: 0,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Где',
                        selectionTitle: userSettings != null
                            ? userSettings.country + ', ' + userSettings.city
                            : '',
                        index: 4,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Где',
                              index: 4,
                              selectedValue: 0,
                              userSettings: userSettings);
                        },
                      ),
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 60),
                      child: RoundButton('Найти', () async {
                        await _auth.signInAnon();
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void routeToSettingsView(
      {String title,
      int index,
      int selectedValue,
      UserSearchModel userSettings}) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SearchSettingsScreen(
                navTitle: title,
                settingType: SettingType.values[index],
                selectedType: selectedValue,
                userSettings: userSettings,
              ),
          fullscreenDialog: true),
    );
  }
}
