import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:social_connect/controller/tabs/firts_tab/searchSettingsScreen.dart';
import 'package:social_connect/controller/tabs/tabBarScreen.dart';
import 'package:social_connect/controller/terms/termsScreen.dart';
import 'package:social_connect/controller/widgets/addad_cell.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/round_button.dart';
import 'package:social_connect/controller/widgets/text_style.dart';
import 'package:social_connect/model/postData.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/model/userSeetingEnum.dart';
import 'package:social_connect/servise/database.dart';

class AddAdScreen extends StatefulWidget {
  @override
  _AddAdScreenState createState() => _AddAdScreenState();
}

class _AddAdScreenState extends State<AddAdScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _email = '';
  String _text = '';
  bool _setEmailFirstTime = true;

  _showSnackBar() {
    final snackBar = SnackBar(
      content: Text(
        'Текст объявления не может быть пустым или содержать одно слово!',
        textAlign: TextAlign.center,
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final userSettings = Provider.of<UserSearchModel>(context);
    setState(() {
      if (userSettings.email.length > 0) {
        if (_setEmailFirstTime) {
          _setEmailFirstTime = false;
          _email = userSettings.email;
        }
      }
    });
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Добавить объявление'),
        backgroundColor: Colors.transparent,
      ),
      body: SafeAreaBaseContainer(
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.82,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: TextField(
                            onChanged: (val) {
                              setState(() => _text = val);
                            },
                            maxLength: 200,
                            maxLines: null,
                            cursorColor: buttonSelectionColor,
                            decoration: InputDecoration(
                              hintText: 'Текст объявления',
                              hintStyle: TextStyle(color: buttonSelectionColor),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 12),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(18)),
                            )),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Form(
                          // autovalidate: true,
                          key: _formKey,
                          child: TextFormField(
                              validator: validateEmail,
                              // validator: (val) => val.isEmpty ? 'Введите email' : null,
                              onChanged: (val) {
                                setState(() => _email = val);
                              },
                              cursorColor: buttonSelectionColor,
                              initialValue: _email.length > 0 ? _email : null,
                              decoration: InputDecoration(
                                hintText: 'Ваша электронная почта',
                                hintStyle:
                                    TextStyle(color: buttonSelectionColor),
                                prefixIcon: Icon(
                                  Icons.email,
                                  color: Colors.grey,
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 2.0, horizontal: 12),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(18)),
                              )),
                        ),
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Сделать объявление VIP'),
                                TextSubtitle(
                                  text:
                                      'Будет выделенно и раслополаться вверху.',
                                  isSubStyle: true,
                                ),
                              ],
                            ),
                            CupertinoSwitch(
                              onChanged: (result) {},
                              value: false,
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      AddAdCell(
                        title: 'Кто вы',
                        selectionTitle: userSettings != null
                            ? WhoAreYou.values[userSettings.howAreYou].title
                            : '',
                        index: 0,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Кто вы',
                              index: 0,
                              selectedValue: userSettings.howAreYou,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Кого ищете',
                        selectionTitle: userSettings != null
                            ? WhoAreYouLookFor
                                .values[userSettings.whoAreYouLooFor].title
                            : '',
                        index: 1,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Кого ищете',
                              index: 1,
                              selectedValue: userSettings.whoAreYouLooFor,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Цель знакомства',
                        selectionTitle: userSettings != null
                            ? YourGoal.values[userSettings.goal].title
                            : '',
                        index: 2,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Цель знакомства',
                              index: 2,
                              selectedValue: userSettings.goal,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'В возрасте',
                        selectionTitle: userSettings != null
                            ? 'От ${userSettings.ageFrom} до ${userSettings.ageTo} лет'
                            : '',
                        index: 3,
                        onTap: () {
                          routeToSettingsView(
                              title: 'В возрасте',
                              index: 3,
                              selectedValue: 0,
                              userSettings: userSettings);
                        },
                      ),
                      AddAdCell(
                        title: 'Где',
                        selectionTitle: userSettings != null
                            ? userSettings.country + ', ' + userSettings.city
                            : '',
                        index: 4,
                        onTap: () {
                          routeToSettingsView(
                              title: 'Где',
                              index: 4,
                              selectedValue: 0,
                              userSettings: userSettings);
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  Center(
                    child: RoundButton(
                      'Опубликовать',
                      () async {
                        if (_text.length < 10) {
                          _showSnackBar();
                        } else {
                          if (_formKey.currentState.validate()) {
                            userSettings.email = _email;
                            userSettings.saveUser();
                            var newPost = PostData(
                              text: _text,
                              email: _email,
                              whoareyou: userSettings.howAreYou,
                              whoAreYouLookFor: userSettings.whoAreYouLooFor,
                              goal: userSettings.goal,
                              ageFrom: userSettings.ageFrom,
                              ageTo: userSettings.ageTo,
                              country: userSettings.country,
                              city: userSettings.city,
                            );
                            final database =
                                DatabaseService(user: userSettings);
                            await database.createRecord(newPost);
                            BottomNavigationBar navigationBar =
                                globalKey.currentWidget;
                            navigationBar.onTap(3);
                          }
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TermsScreen(),
                                fullscreenDialog: true),
                          );
                        },
                        child: TextSubtitleAligment(
                          text: 'Продолжая вы соглашаетесь\nс нашими правилами',
                          isSubStyle: true,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Введите корректный email';
    else
      return null;
  }

  void routeToSettingsView(
      {String title,
      int index,
      int selectedValue,
      UserSearchModel userSettings}) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SearchSettingsScreen(
                navTitle: title,
                settingType: SettingType.values[index],
                selectedType: selectedValue,
                userSettings: userSettings,
              ),
          fullscreenDialog: true),
    );
  }
}
