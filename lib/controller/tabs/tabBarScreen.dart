import 'package:flutter/material.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:social_connect/controller/tabs/firts_tab/searchAdsScreen.dart';
import 'package:social_connect/controller/tabs/fourth_tab/messageContainer.dart';
import 'package:social_connect/controller/tabs/second_tab/createPostScreen.dart';
import 'package:social_connect/controller/tabs/third_tab/userPostScreen.dart';

//Tabbar key
GlobalKey globalKey = GlobalKey(debugLabel: 'btm_app_bar');

class TabBarScreen extends StatefulWidget {
  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  static List<Widget> _widgetOptions = <Widget>[
    SearchAdsScreen(),
    AddAdScreen(),
    ChatScreen(),
    UserPostScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          key: globalKey,
          backgroundColor: mainViewBackgroundColor,
          type: BottomNavigationBarType.fixed,
          elevation: 0,
          selectedIconTheme: IconThemeData(
            color: buttonSelectionColor,
          ),
          unselectedIconTheme: IconThemeData(
            color: unselectedIconTheme,
          ),
          iconSize: 24,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text(
                "",
                style: TextStyle(fontSize: 0),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add),
              title: Text(
                "",
                style: TextStyle(fontSize: 0),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.forum),
              title: Text(
                "",
                style: TextStyle(fontSize: 0),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.people),
              title: Text(
                "",
                style: TextStyle(fontSize: 0),
              ),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
