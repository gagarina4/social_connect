import 'package:flutter/material.dart';
import 'package:social_connect/color_scheme.dart';

class RoundButton extends StatelessWidget {
  RoundButton(this.title, this.callback);
  final String title;
  final Function callback;
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: callback,
      splashColor: splashButtonColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 12),
        child: Container(
          width: 140,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(title),
              SizedBox(width: 2),
              Icon(
                Icons.arrow_forward_ios,
                size: 13,
              ),
            ],
          ),
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      color: buttonSelectionColor,
      textColor: Color(0xFF242141),
    );
  }
}
