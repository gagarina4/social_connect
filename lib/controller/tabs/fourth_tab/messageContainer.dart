import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:social_connect/controller/tabs/fourth_tab/messagesScreen.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/model/userSearchSettings.dart';

class ChatScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final userSettings = Provider.of<UserSearchModel>(context);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Общение'),
          backgroundColor: scaffoldBacgroundColor,
          bottom: TabBar(
            indicatorSize: TabBarIndicatorSize.label,
            indicatorColor: mainViewBackgroundColor,
            tabs: [
              SizedBox(
                width: 120,
                height: 40,
                child: Center(
                  child: Text(
                    'Мне ответили',
                    style: TextStyle(color: mainViewBackgroundColor),
                  ),
                ),
              ),
              SizedBox(
                width: 120,
                height: 40,
                child: Center(
                  child: Text(
                    'Я ответил',
                    style: TextStyle(color: mainViewBackgroundColor),
                  ),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            SafeAreaBaseContainer(
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: MessagesScreen(userSettings: userSettings, type: 0),
              ),
            ),
            SafeAreaBaseContainer(
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: MessagesScreen(userSettings: userSettings, type: 10),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
