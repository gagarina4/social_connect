import 'package:mailer2/mailer.dart';
import 'dart:io';

const emailOriginal = '';
const emailPass = '';

class Mailer {
  static sendEmail(
      String message, String sendToEmail, File image, Function(bool) result) {
    final options = GmailSmtpOptions()
      ..username = emailOriginal
      ..password = emailPass;

    var emailTransport = SmtpTransport(options);
    emailTransport
        .send(getEnvelope(emailOriginal, message, sendToEmail, image))
        .then((envelope) {
      result(true);
    }).catchError((e) {
      print('Sending e-mail fail with: $e');
      result(false);
    });
  }

  static Envelope getEnvelope(
      String emailOriginal, String message, String sendToEmail, File image) {
    if (image == null) {
      return Envelope()
        ..from = emailOriginal
        ..recipients.add(sendToEmail)
        ..subject = 'Сообщение от Rubber'
        ..text = message;
    } else {
      return Envelope()
        ..from = emailOriginal
        ..recipients.add(sendToEmail)
        ..subject = 'Сообщение от Rubber'
        ..text = message
        ..attachments.add(Attachment(file: image));
    }
  }
}
