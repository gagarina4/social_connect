import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:social_connect/color_scheme.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: SizedBox(
          width: 50,
          height: 50,
          child: LoadingIndicator(
            indicatorType: Indicator.ballScaleRippleMultiple,
            color: buttonSelectionColor,
          ),
        ),
      ),
    );
  }
}
