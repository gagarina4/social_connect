import 'package:flutter/material.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:social_connect/controller/widgets/base_safearea_container.dart';
import 'package:social_connect/controller/widgets/loading.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'dart:async';

import 'package:social_connect/model/userSeetingEnum.dart';
import 'package:social_connect/servise/database.dart';

class SearchSettingsScreen extends StatefulWidget {
  SearchSettingsScreen(
      {@required this.navTitle,
      @required this.settingType,
      @required this.selectedType,
      @required this.userSettings});
  final String navTitle;
  final SettingType settingType;
  final int selectedType;
  final UserSearchModel userSettings;
  @override
  _SearchSettingsScreenState createState() => _SearchSettingsScreenState();
}

class _SearchSettingsScreenState extends State<SearchSettingsScreen> {
  int _fromAgeValue = 18;
  int _toAgeValue = 35;

  @override
  void initState() {
    super.initState();
    _fromAgeValue = widget.userSettings.ageFrom;
    _toAgeValue = widget.userSettings.ageTo;
  }

  Future<List<Widget>> _getData() async {
    var result = await DatabaseService(user: widget.userSettings).countries;
    List<Widget> items = [];
    var index = 0;
    if (widget.settingType == SettingType.place) {
      for (var item in result) {
        items.add(SelectionCell(
            title: item.name,
            isSelected: widget.userSettings.country == item.name ? true : false,
            index: index,
            onTap: (index) {
              routeToSettingsView(
                  title: 'Выберите город', index: 5, selectedValue: 0);
            }));
        // }
        index++;
      }
    } else {
      for (var item in result) {
        if (widget.userSettings.country == item.name) {
          for (var city in item.cities) {
            items.add(SelectionCell(
                title: city,
                isSelected: widget.userSettings.city == city ? true : false,
                index: index,
                onTap: (index) {
                  print('index is: $index');
                  widget.userSettings.city = item.cities[index];
                  widget.userSettings.saveUser();
                  Navigator.of(context).popUntil((route) => route.isFirst);
                }));
            index++;
          }
        }
      }
    }
    return items;
  }

  void routeToSettingsView({String title, int index, int selectedValue}) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SearchSettingsScreen(
          navTitle: title,
          settingType: SettingType.values[index],
          selectedType: selectedValue,
          userSettings: widget.userSettings,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.navTitle),
        backgroundColor: Colors.transparent,
      ),
      body: SafeAreaBaseContainer(
        child: Padding(
          padding: const EdgeInsets.only(top: 18),
          child: getWidgetForType(),
        ),
      ),
    );
  }

  Widget getWidgetForType() {
    switch (widget.settingType) {
      case SettingType.whoareyou:
      case SettingType.whoareyouloofor:
      case SettingType.yourGoal:
        return preinstallValuesBuilder();
      case SettingType.ages:
        return agesValuesBuilder();
      default:
        return asyncValuesBuilder();
    }
  }

  Widget preinstallValuesBuilder() {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) => getTypeCell()[index],
      itemCount: getTypeCell().length,
    );
  }

  List<Widget> getTypeCell() {
    List<Widget> widgets = [];
    var index = 0;
    for (var item in widget.settingType.currentCellTypes()) {
      widgets.add(SelectionCell(
          title: item.title,
          isSelected: widget.selectedType == item.index ? true : false,
          index: index,
          onTap: (index) {
            switch (widget.settingType) {
              case SettingType.whoareyou:
                widget.userSettings.howAreYou = index;
                break;
              case SettingType.whoareyouloofor:
                widget.userSettings.whoAreYouLooFor = index;
                break;
              case SettingType.yourGoal:
                widget.userSettings.goal = index;
                break;
              default:
            }
            widget.userSettings.saveUser();
            Navigator.pop(context);
          }));
      index++;
    }
    return widgets;
  }

  Widget agesValuesBuilder() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      alignment: Alignment.topCenter,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 44,
            left: 0,
            right: 0,
            child: Divider(
              color: white_01,
            ),
          ),
          Positioned(
            top: 90,
            left: 0,
            right: 0,
            child: Divider(
              color: white_01,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('От'),
              SizedBox(
                width: 10,
              ),
              Theme(
                data: ThemeData(
                  textTheme: TextTheme(
                    body1: TextStyle(
                      color: buttonSelectionColor,
                      fontFamily: 'OpenSans',
                    ),
                    headline: TextStyle(
                      fontFamily: 'OpenSans',
                    ),
                  ),
                  accentColor: buttonSelectionColor,
                ),
                child: NumberPicker.integer(
                  onChanged: (value) => setState(
                    () => setState(() {
                      widget.userSettings.ageFrom = value;
                      widget.userSettings.saveUser();
                      _fromAgeValue = value;
                    }),
                  ),
                  minValue: 18,
                  maxValue: 99,
                  initialValue: _fromAgeValue,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text('до'),
              SizedBox(
                width: 10,
              ),
              Theme(
                data: ThemeData(
                  textTheme: TextTheme(
                    body1: TextStyle(
                      color: buttonSelectionColor,
                      fontFamily: 'OpenSans',
                    ),
                    headline: TextStyle(
                      fontFamily: 'OpenSans',
                    ),
                  ),
                  accentColor: buttonSelectionColor,
                ),
                child: NumberPicker.integer(
                  onChanged: (value) => setState(
                    () => setState(() {
                      widget.userSettings.ageTo = value;
                      widget.userSettings.saveUser();
                      _toAgeValue = value;
                    }),
                  ),
                  minValue: 18,
                  maxValue: 99,
                  initialValue: _toAgeValue,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text('лет'),
            ],
          ),
        ],
      ),
    );
  }

  Widget asyncValuesBuilder() {
    return FutureBuilder(
      future: _getData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        // print(snapshot.data);
        if (snapshot.data == null) {
          return Loading();
        } else {
          return ListView.builder(
            itemBuilder: (BuildContext context, int index) =>
                snapshot.data[index],
            itemCount: snapshot.data.length,
          );
        }
      },
    );
  }
}

class SelectionCell extends StatelessWidget {
  SelectionCell(
      {@required this.title,
      @required this.index,
      this.isSelected,
      this.subtitle,
      @required this.onTap});
  final String title;
  final int index;
  final bool isSelected;
  final String subtitle;
  final Function(int index) onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: splashColor,
        highlightColor: Colors.transparent,
        onTap: () {
          onTap(index);
        },
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                    child: subtitle == null
                        ? Text(title,
                            style: isSelected
                                ? TextStyle(color: buttonSelectionColor)
                                : null)
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(title,
                                  style: isSelected
                                      ? TextStyle(color: buttonSelectionColor)
                                      : null),
                              Text(subtitle,
                                  style: isSelected
                                      ? TextStyle(color: buttonSelectionColor)
                                      : null),
                            ],
                          ),
                  ),
                  isSelected
                      ? Icon(
                          Icons.check,
                          size: 18,
                          color: buttonSelectionColor,
                        )
                      : SizedBox()
                ],
              ),
            ),
            Divider(
              height: 1,
              thickness: 1,
              color: isSelected ? buttonSelectionColor : null,
            ),
          ],
        ),
      ),
    );
  }
}
