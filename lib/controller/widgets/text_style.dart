import 'package:flutter/material.dart';

class TextSubtitleAligment extends StatelessWidget {
  final String text;
  final bool isSubStyle;
  TextSubtitleAligment({this.text, this.isSubStyle});

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: TextAlign.center,
        style: isSubStyle ? getSubStyle() : getTitleStyle());
  }

  TextStyle getSubStyle() {
    return TextStyle(
      color: Colors.white30,
      fontSize: 12,
    );
  }

  TextStyle getTitleStyle() {
    return TextStyle(
      color: Colors.white54,
      fontSize: 13,
      fontWeight: FontWeight.w100,
      letterSpacing: 0.6,
    );
  }
}

class TextSubtitle extends StatelessWidget {
  final String text;
  final bool isSubStyle;
  final Color textColor;
  TextSubtitle({this.text, this.isSubStyle, this.textColor});

  @override
  Widget build(BuildContext context) {
    return Text(text, style: isSubStyle ? getSubStyle() : getTitleStyle());
  }

  TextStyle getSubStyle() {
    return TextStyle(
      color: textColor != null ? textColor : Colors.white30,
      fontSize: 12,
    );
  }

  TextStyle getTitleStyle() {
    return TextStyle(
      color: Colors.white54,
      fontSize: 13,
      fontWeight: FontWeight.w100,
      letterSpacing: 0.6,
    );
  }
}
