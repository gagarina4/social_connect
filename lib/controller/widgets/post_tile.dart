import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_connect/color_scheme.dart';
import 'package:social_connect/controller/tabs/firts_tab/adInfoScreen.dart';
import 'package:social_connect/controller/widgets/text_style.dart';
import 'package:social_connect/model/postData.dart';
import 'package:social_connect/model/userSearchSettings.dart';
import 'package:social_connect/model/userSeetingEnum.dart';
import 'package:social_connect/servise/time.dart';

class PostTile extends StatelessWidget {
  PostTile({this.post, @required this.type});
  final int type;
  final PostData post;

  @override
  Widget build(BuildContext context) {
    final userSettings = Provider.of<UserSearchModel>(context);
    return Container(
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          highlightColor: Colors.transparent,
          splashColor: splashColor,
          child: Padding(
            padding: const EdgeInsets.all(14.0),
            child: cellFor(post: post),
          ),
          onTap: () {
            if (type == 0) {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        AdInfoScreen(post: post, userSettings: userSettings),
                    fullscreenDialog: true),
              );
            } else {}
          },
        ),
      ),
    );
  }

  Widget cellFor({PostData post}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(
            child: TextSubtitle(
              text: Time.readTimestamp(post.date.millisecondsSinceEpoch),
              isSubStyle: false,
            ),
          ),
          SizedBox(height: 4),
          Text('${post.text}'),
          SizedBox(height: 2),
          TextSubtitle(
            text: WhoAreYou.values[post.whoareyou].title +
                ', ищет ' +
                WhoAreYouLookFor.values[post.whoAreYouLookFor].title,
            isSubStyle: true,
          ),
          SizedBox(height: 1),
          Container(
            decoration: BoxDecoration(
              color: scaffoldBacgroundColor,
              borderRadius: BorderRadius.circular(4),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(6, 2, 6, 2),
              child: TextSubtitle(
                text: YourGoal.values[post.goal].title,
                isSubStyle: true,
                textColor: mainViewBackgroundColor,
              ),
            ),
          ),
          SizedBox(height: 1),
          TextSubtitle(
            text: post.country + ', ' + post.city,
            isSubStyle: true,
          ),
        ],
      ),
    );
  }

//Девушка 32 года, ищет парня
//Секс
//Страна, город

}
